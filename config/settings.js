const rootURL = 'https://randomuser.me/'

const settings = {
  url: rootURL
}

settings.regenerateConfig =  () => {
  settings.api = settings.url + '/api'
}

settings.regenerateConfig()

export default settings
