import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import DetailsPage from "../pages/Details.page.vue";
import UserDetails from "../components/UserDetails.vue";
import MainPage from "../pages/MainPage.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "MainPage",
    component: MainPage,
  },
  {
    path: '/search',
    component: DetailsPage,
    meta: {
      title: 'Details Page',
    },
    children: [
      {
        path: 'user/:id',
        component: UserDetails,
        meta: {
          title: 'User Details',
        }
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
