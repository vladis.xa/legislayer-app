import { createApp } from 'vue'
import router from './router/index'

import ElementPlus from 'element-plus';
import store from './store/index';

import 'element-plus/lib/theme-chalk/index.css';
import "./assets/styles/main.scss";

import App from './App.vue'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.use(store)
app.mount('#app');
