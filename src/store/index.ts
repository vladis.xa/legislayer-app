import { createStore } from 'vuex'
import users from './modules/users'

let store = createStore({
  modules: {
    users
  },
});

export default store
