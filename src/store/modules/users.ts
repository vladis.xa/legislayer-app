import axios from "axios";
import settings from "@/../config/settings";
import UsersList from "@/models/UsersListModel.ts";

const state = () => ({
  page: 0,
  list: new Array<UsersList>(),
  sortedList: new Array<UsersList>(),
  searchBy: {
    string: '',
    gender: ''
  }
});

// mutations
const mutations = {
  setUsersList (state, payload) {
    state.list = [...state.list, ...payload]
    state.sortedList = state.list
  },
  setPage (state, payload) {
    state.page = payload
  },
  searchByString (state, payload) {
    state.searchBy.string = payload
  },
  searchByGender (state, payload) {
    state.searchBy.gender = payload
  }
};

// actions
const actions = {

  async getUsersList ({ commit, state }, payload) {
    const {page, results} = payload
    try {
      const response = await axios.get(settings.api + `/?page=${page}&results=${results}`)
      if (response && response.status === 200) {
        commit('setUsersList', response.data.results)
        commit('setPage', page)
      }
    } catch (e) {
      console.log(e)
    }
  }
};

// getters
const getters = {
  getUserById: (state) => (id) => {
    return state.sortedList.find(user => user.id.value === id)
  },
  getSortedUsers: (state) => () => {
    if (!state.list.length) return []
    let search = state.searchBy.string
    let gender = state.searchBy.gender
    return state.list.filter(user =>  {
      if ((user.name.first.toLowerCase().indexOf(search) >= 0 || user.name.last.toLowerCase().indexOf(search) >= 0)) {
        if (user.gender === gender || gender === '') {
          return true
        }
      }
      return  false
    })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
