export interface Id {
  name: string;
  value: string;
}

export interface Street {
  name: string;
  number: number;
}

export interface Location {
  city: string;
  country: string;
  postcode: number;
  state: string;
  street: {
    [key: string]: Street
  };
}

export interface Name {
  first: string;
  last: string;
  title: string;
}

export interface Picture {
  large: string;
  medium: string;
  thumbnail: string;
}

export default interface User {
  cell: string;
  email: string;
  gender: string;
  id: {
    [key: string]: Id
  };
  location: {
    [key: string]: Location
  };
  name: {
    [key: string]: Name
  };
  picture:  {
    [key: string]: Picture
  };
}
