import User from './UserModel';

export default interface UsersList {
  data: Array<User>;
}
